package db;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class BUSRatesDatabase extends Mysql implements BusesDatabaseAPI {
    private static volatile BUSRatesDatabase mInstance;
    private Map<String, Map<String, Double>> payload = new HashMap<>();

    private BUSRatesDatabase() {
        initConnection();
    }

    public static BUSRatesDatabase getInstance() {
        if (mInstance == null) {
            synchronized (BUSRatesDatabase.class) {
                if (mInstance == null) {
                    mInstance = new BUSRatesDatabase();
                }
            }
        }
        return mInstance;
    }

    @Override
    public void connect() {
        initConnection();
    }

    @Override
    public void disconnect() {

    }


    @Override
    public boolean checkBusExists(String bus) {
        ResultSet rs = select("SELECT COUNT(`id`) as `count` FROM `buses` WHERE `number`='"+bus+"'");
        try {
            while (rs.next()) {
                return (rs.getInt("count")>0?true:false);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void insertBusCourse(String bus, String time_from, String time_to){
        ResultSet rs = select("SELECT `id` FROM `buses` WHERE `number`='"+bus+"'");
        int bus_ID = 0;
        try {
            while (rs.next()) {
                bus_ID = rs.getInt("id");
            }

            if(bus_ID>0){
                if(!insert("INSERT INTO `courses`(`bus_id`,`time_from`,`time_to`)VALUES('"+bus_ID+"','"+time_from+"','"+time_to+"')")){
                    System.out.println("Error on adding course");
                }
            }

        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertBus(String bus) {
        if(!insert("INSERT INTO `buses` (`number`) VALUES ('"+bus+"')")){
            System.out.println("Error on adding bus");
        }
    }


    @Override
    public void updateBusesCourses(String cesp, Map<String, Double> buses) {
        Map<String, Double> oldBuses = payload.get(cesp);
        buses.forEach(oldBuses::put);
    }

    @Override
    public boolean deleteCourse(String bus, String time_from, String time_to) {
        ResultSet rs = select("SELECT `id` FROM `buses` WHERE `number`='"+bus+"'");
        int bus_ID = 0;
        try {
            while (rs.next()) {
                bus_ID = rs.getInt("id");
            }

            if(bus_ID>0){
                return insert("DELETE FROM `courses` WHERE `bus_id`='"+bus_ID+"' AND `time_from` LIKE '%"+time_from+"%' AND `time_to` LIKE '%"+time_to+"%'");
            }

        }catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteBusCourses(String bus) {
        ResultSet rs = select("SELECT `id` FROM `buses` WHERE `number`='"+bus+"'");
        int bus_ID = 0;
        try {
            while (rs.next()) {
                bus_ID = rs.getInt("id");
            }

            if(bus_ID>0){
                return insert("DELETE FROM `courses` WHERE `bus_id`='"+bus_ID+"'");
            }

        }catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteBus(String bus) {
        return insert("DELETE FROM `buses` WHERE `number`='"+bus+"'");
    }

    @Override
    public Map<String, String> selectBusesCourses(String bus) {
        Map<String, String> rates = new HashMap<>();
        ResultSet rs = select("SELECT `courses`.`time_from`, `courses`.`time_to` FROM `courses` " +
                "LEFT JOIN `buses` ON `buses`.`id`=`courses`.`bus_id` WHERE `buses`.`number`='"+bus+"'");
        try {
            while (rs.next()) {
                rates.put(rs.getString("time_from"),rs.getString("time_to"));
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return rates;
    }

    @Override
    public Map<String, Map<String, String>> selectBusesCourses() {
        Map<String, Map<String, String>> resultMap = new HashMap<>();
        ResultSet rs = select("SELECT `courses`.`time_from`, `courses`.`time_to`, `buses`.`number` as `bus` FROM `courses` LEFT JOIN `buses` ON `buses`.`id`=`courses`.`bus_id` ORDER BY `courses`.`bus_id`");
        try {
            while (rs.next()) {
                String bus = rs.getString("bus");
                if (!resultMap.keySet().contains(bus)){
                    Map<String, String> new_bus = new HashMap<>();
                    new_bus.put(rs.getString("time_from"),rs.getString("time_to"));
                    resultMap.put(bus,new_bus);
                }
                else{
                    resultMap.get(bus).put(rs.getString("time_from"),rs.getString("time_to"));
                }
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return resultMap;
    }
}
