package db;

import java.util.Map;
import java.util.Set;

public interface BusesDatabaseAPI {
    
    void connect();
    
    void disconnect();

    boolean checkBusExists(String bus);

    void insertBusCourse(String bus, String time_from, String time_to);

    void insertBus(String bus);

    void updateBusesCourses(String cesp, Map<String, Double> buses);

    boolean deleteCourse(String bus, String time_from, String time_to);

    boolean deleteBusCourses(String bus);

    boolean deleteBus(String bus);

    /**
     *
     * @param bus desired bus
     * @return all hours offers for given course as map
     */
    Map<String, String> selectBusesCourses(String bus);

    /**
     * Get all available currency rates for today`s date
     * @return Currency Exchange Service Provider (CESP) mapped to it`s rates
     */
    Map<String, Map<String, String>> selectBusesCourses();
}

