package db;

import java.sql.*;

public class Mysql {
    Connection conn = null;
    Statement stmt;

    protected void initConnection() {
        try {
            conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/microbuses?" +
                    "user=root&password=123456");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected ResultSet select(String query){
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            return  rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected boolean insert(String query){
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            return  true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
