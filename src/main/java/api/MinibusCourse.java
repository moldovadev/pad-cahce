package api;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import db.BUSRatesDatabase;
import redis.clients.jedis.Jedis;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Path("courses")
public class MinibusCourse {
    private BUSRatesDatabase db = BUSRatesDatabase.getInstance();
    private Jedis jedis = new Jedis("localhost");
    private Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .create();

    private static final Response NOT_IMPLEMENTED = Response
            .status(501)
            .entity("This operation is not yet implemented")
            .build();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{course}")
    public Response getHoursForBus(@PathParam("course") String bus) {
        Map<String, String> rates = null;
        rates = db.selectBusesCourses(bus);
        jedis.hmset("course/"+bus,rates);
        if (rates == null || rates.isEmpty()) {
            return Response.ok("There is no courses for bus: " + bus).build();
        }
        return Response.ok(this.mapToJson(rates)).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHoursForAllBuses() {
        Map<String, Map<String, String>> rates = db.selectBusesCourses();
        if (rates == null || rates.isEmpty()) {
            return Response.ok("There is no any available buses and courses. Please repeat later").build();
        }
        return Response.ok(this.mapToJson(rates)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addBusesCourses(@QueryParam("bus") String bus, @QueryParam("from") String from, @QueryParam("to") String to) {
        db.insertBusCourse(bus, from, to);
        return Response.ok(this.mapToJson(db.selectBusesCourses(bus))).build();
    }

    @PATCH
    @Path("{bus}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response patchBusesCourses(@PathParam("bus") String bus) {
        db.deleteBus(bus);
        return Response.ok("success").build();
    }

    @DELETE
    @Path("{bus}")
    public Response removeBusesCourses(@PathParam("bus") String bus) {
        db.deleteBusCourses("bus");
        return Response.ok("success").build();
    }

    private String mapToJson(Map<?, ?> map) {
        return gson.toJson(map);
    }
}
